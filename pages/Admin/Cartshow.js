import React, { useState,useMemo } from 'react';
import { AgGridReact } from 'ag-grid-react';
import { render } from 'react-dom';

import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

export default function Cartshow(props) {
 
    const {info} = props;
    const defaultColDef = useMemo( ()=> ({
        sortable: true
      }));    
    
        
    const [columnDefs] = useState([
        { field: 'Name',filter: true},
        { field: 'Age' ,filter: true},
        { field: 'Address',filter: true},
        
    ])
 
    return (
        <div className="ag-theme-alpine" style={{height: 600, width: 1300}}>
            
        <AgGridReact
      rowData={info.map((data) => ({Name: data.todos.name, Age: data.todos.age, Address: data.todos.address}))}
      rowSelection='multiple'
      animateRows={true}
      defaultColDef={defaultColDef}
      columnDefs={columnDefs}>
                
            </AgGridReact>
        </div>
    );
    }
