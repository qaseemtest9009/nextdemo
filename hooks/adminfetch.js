import React, { useState, useEffect, useRef } from 'react'
import { collection,doc, getDocs } from 'firebase/firestore'
import { useAuth } from '../context/AuthContext'
import { db } from '../firebase'

export default function adminfetch() {
 
    const [info , setInfo] = useState([]);
    
    useEffect(() => {
        async function fetchData1() {
            try {
                
                getDocs(collection(db,'users')).then((snapshot)=>{
                    setInfo(snapshot.docs.map((doc) =>({...doc.data()})));
                  
        // const querySnapshot = await getDocs(collection(db, "users"));
      
        // querySnapshot.forEach((doc) => {
        //     var data = doc.data();
        //     setInfo(arr => [...arr , data])
        });
    }
    catch{
    
    }}
    
    fetchData1()
    }, [0])

    return {info,setInfo }
}