import { initializeApp } from 'firebase/app';
import {getAuth} from "firebase/auth";
import {getFirestore} from "firebase/firestore";
// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
    apiKey: "AIzaSyD5jUi6dtf4dPFOhMi9ZFZoLUfXCdtqd40",
    authDomain: "nextdemo-c7d2d.firebaseapp.com",
    projectId: "nextdemo-c7d2d",
    storageBucket: "nextdemo-c7d2d.appspot.com",
    messagingSenderId: "805658074033",
    appId: "1:805658074033:web:d493524e35a390e3523b90",
    measurementId: "G-YZ5ZZPGXSN"

};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
export const db = getFirestore(app)
